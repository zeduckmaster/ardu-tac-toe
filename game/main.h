#pragma once
#include "zarduboy.h"
#include "font.h"
#include "gfx.h"
#include "text.h"

#if defined(__linux__) || defined(__APPLE__)
#include <cstdio>
finline char* itoa(int value, char* str, int base)
{
	switch(base)
	{
	case 10: sprintf(str, "%d", value); break;
	}
	return str;
}
#endif

////////////////////////////////////////////////////////////////////////////////
// types

struct Point
{
	int16_t x;
	int16_t y;
};
static_assert(sizeof(Point) == sizeof(int16_t) * 2, "error");

struct Line
{
	Point p0;
	Point p1;
};
static_assert(sizeof(Line) == sizeof(Point) * 2, "error");

using FuncGameState = void(*)();

enum class eGameMode : uint8_t
{
	VsCPUEasy = 0,
	VsCPUNormal,
	Vs
};

enum class eGameState : uint8_t
{
	InitBoard = 0,
	Playing,
	SwitchingPlayer,
	AIPlaying,
	Finished
};

enum class eGamePlayerMark : uint8_t
{
	None = 0,
	Cross,
	Circle
};

// handles a single board case (state, anim, etc.)
struct BoardCase
{
	Point pos;
	eGamePlayerMark state;
	Line drawLines[8];
	int8_t drawAnimFrames;

	BoardCase(int16_t x, int16_t y)
		: pos{x, y}
	{}
};

// handles the pencil, i.e.: the current player
struct Pencil
{
	int8_t boardPos;
	eGamePlayerMark playerMark = eGamePlayerMark::Cross;
	bool isMarking;
	bool needSwitchPlayer;
};

// handles the board logic
struct Board
{
	Line lines[4];
	Line winLine;
	int8_t emptyCount;
	bool needCheckMatch3;
};

////////////////////////////////////////////////////////////////////////////////
// globals

ZArduboy Z;

constexpr auto const FrameTime             = int32_t{1000/60}; // 60fps
//constexpr auto const EEPROMMagicNumber     = uint8_t{0xF2}; // game specific code
//constexpr auto const EEPROMMagicNumber_pos = uint16_t{32};
//constexpr auto const EEPROMSound_pos       = uint16_t{33};
//constexpr auto const EEPROMHiScore_pos = uint16_t{34};

auto lasttime = uint32_t{0};
auto frame = uint32_t{0};
auto previousButtonState = uint8_t{0};
auto gameFunc = FuncGameState{nullptr}; // main game function that is run every frame
auto gameAnimFrames = int16_t{0}; // used for global animations
eGameMode gameMode; // what is chosen by the player
eGameState gameState;
eGamePlayerMark gameWinner;
int8_t menuCursorIndex = 0;
Pencil pencil;
Board board;

// a hand-drawn circle is made of 8 segments
Line const handCircleLines[8] =
{
	{{-2, -6}, { 2, -6}},
	{{ 2, -6}, { 6, -2}},
	{{ 6, -2}, { 6,  2}},
	{{ 6,  2}, { 2,  6}},
	{{ 2,  6}, {-2,  6}},
	{{-2,  6}, {-6,  2}},
	{{-6,  2}, {-6, -2}},
	{{-6, -2}, {-2, -6}}
};

// the board data (9 cases)
BoardCase boardCases[] =
{
	BoardCase{10, 10}, BoardCase{31, 10}, BoardCase{53, 10},
	BoardCase{10, 31}, BoardCase{31, 31}, BoardCase{53, 31},
	BoardCase{10, 53}, BoardCase{31, 53}, BoardCase{53, 53}
};

// used to check every possible match 3 of the board (easier than a more complex loop)
uint8_t const boardCheck[] PROGMEM =
{
	0, 1, 2,
	3, 4, 5,
	6, 7, 8,
	0, 3, 6,
	1, 4, 7,
	2, 5, 8,
	0, 4, 8,
	2, 4, 6
};

////////////////////////////////////////////////////////////////////////////////
// game funcs

void drawNumbers(int16_t x, int16_t y, uint16_t value)
{
	char buf[6];
	itoa(value, buf, 10);
	auto len = strlen(buf);
	for(auto n = size_t{0}; n < len; ++n)
	{
		auto digit = static_cast<uint8_t>(buf[n] - '0');
		Z.drawBitmap(x + n * (FontWidth + 1), y, fontNumbers + digit * FontWidth, FontWidth, FontHeight);
	}
}

void drawText(int16_t x, int16_t y, char const* text)
{
	auto p = text;
	auto px = x;
	while(*p != '\0')
	{
		if(*p != ' ')
		{
			auto c = (*p - 'A') * FontWidth;
			Z.drawBitmap(px, y, fontLetters + c, FontWidth, FontHeight);
		}
		px += (FontWidth + 1);
		++p;
	}
}

void drawTextP(int16_t x, int16_t y, char const* text)
{
	//auto p = text;
	auto px = x;
	auto c = static_cast<char>(pgm_read_byte(text++));
	while(c != '\0')
	{
		if(c != ' ')
		{
			auto rc = (c - 'A') * FontWidth;
			Z.drawBitmap(px, y, fontLetters + rc, FontWidth, FontHeight);
		}
		px += (FontWidth + 1);
		c = static_cast<char>(pgm_read_byte(text++));
	}
}

void drawPlayerName(int16_t x, int16_t y, eGamePlayerMark player)
{
	//drawText(x, y, "PLAYER\0");
	drawTextP(x, y, str_PLAYER);
	if(player == eGamePlayerMark::Cross)
		drawText(x + 28, y, "X\0");
	else
		drawText(x + 28, y, "O\0");
}

finline bool isAnimated(BoardCase const& boardCase)
{
	return boardCase.drawAnimFrames > 0;
}

finline bool isEmpty(BoardCase const& boardCase)
{
	return boardCase.state == eGamePlayerMark::None;
}

void initBoardCase(BoardCase& boardCase)
{
	boardCase.state = eGamePlayerMark::None;
	boardCase.drawAnimFrames = 0;
}

void updateBoardCase(BoardCase& boardCase)
{
	if(boardCase.drawAnimFrames > 0)
		--boardCase.drawAnimFrames;
}

void drawBoardCase(BoardCase& boardCase)
{
	if(isEmpty(boardCase) == true)
		return;
	if(boardCase.state == eGamePlayerMark::Cross)
	{
		auto nline = 2 - boardCase.drawAnimFrames / 15;
		auto& l0 = boardCase.drawLines[0];
		auto& l1 = boardCase.drawLines[1];
		Z.drawLine(l0.p0.x, l0.p0.y, l0.p1.x, l0.p1.y, eColor::White);
		if(nline > 1)
			Z.drawLine(l1.p0.x, l1.p0.y, l1.p1.x, l1.p1.y, eColor::White);
	}
	else
	{
		auto nline = 8 - boardCase.drawAnimFrames / 3;
		auto& pos = boardCase.pos;
		for(auto n = 0; n < nline; ++n)
		{
			auto& line = boardCase.drawLines[n];
			Z.drawLine(pos.x + line.p0.x, pos.y + line.p0.y, pos.x + line.p1.x, pos.y + line.p1.y, eColor::White);
		}
	}
}

void markCross(BoardCase& boardCase)
{
	boardCase.drawAnimFrames = 2 * 15;
	auto maxShift = int32_t{2};
	boardCase.drawLines[0].p0.x = static_cast<int16_t>(boardCase.pos.x - 6 + random(-maxShift, maxShift));
	boardCase.drawLines[0].p0.y = static_cast<int16_t>(boardCase.pos.y - 6 + random(-maxShift, maxShift));
	boardCase.drawLines[0].p1.x = static_cast<int16_t>(boardCase.pos.x + 6 + random(-maxShift, maxShift));
	boardCase.drawLines[0].p1.y = static_cast<int16_t>(boardCase.pos.y + 6 + random(-maxShift, maxShift));
	boardCase.drawLines[1].p0.x = static_cast<int16_t>(boardCase.pos.x - 6 + random(-maxShift, maxShift));
	boardCase.drawLines[1].p0.y = static_cast<int16_t>(boardCase.pos.y + 6 + random(-maxShift, maxShift));
	boardCase.drawLines[1].p1.x = static_cast<int16_t>(boardCase.pos.x + 6 + random(-maxShift, maxShift));
	boardCase.drawLines[1].p1.y = static_cast<int16_t>(boardCase.pos.y - 6 + random(-maxShift, maxShift));
	boardCase.state = eGamePlayerMark::Cross;
}

void markCircle(BoardCase& boardCase)
{
	boardCase.drawAnimFrames = 8 * 3;
	auto maxShift = int32_t{1};
	{
		auto hl = &handCircleLines[0];
		auto& dl = boardCase.drawLines[0];
		dl.p0.x = static_cast<int16_t>(hl->p0.x + random(-maxShift, maxShift));
		dl.p0.y = static_cast<int16_t>(hl->p0.y + random(-maxShift, maxShift));
	}
	for(auto n = 0; n < 8; ++n)
	{
		auto hl = &handCircleLines[n];
		auto& dl = boardCase.drawLines[n];
		dl.p1.x = static_cast<int16_t>(hl->p1.x + random(-maxShift, maxShift));
		dl.p1.y = static_cast<int16_t>(hl->p1.y + random(-maxShift, maxShift));
		if(n != 7)
			boardCase.drawLines[n+1].p0 = dl.p1;
	}
	boardCase.state = eGamePlayerMark::Circle;
}

void initPencil()
{
	pencil.boardPos = 1;
	pencil.isMarking = false;
	pencil.needSwitchPlayer = false;
}

finline BoardCase& currentBoardCase()
{
	return boardCases[pencil.boardPos];
}

finline void switchPlayer()
{
	pencil.playerMark = (pencil.playerMark == eGamePlayerMark::Cross)? eGamePlayerMark::Circle : eGamePlayerMark::Cross;
	pencil.needSwitchPlayer = false;
}

void movePencilLeft()
{
	auto bp = static_cast<int8_t>(pencil.boardPos - 1);
	if(bp < 0)
		bp = 8;
	pencil.boardPos = bp;
}

void movePencilRight()
{
	auto bp = static_cast<int8_t>(pencil.boardPos + 1);
	if(bp > 8)
		bp = 0;
	pencil.boardPos = bp;
}

void movePencilUp()
{
	auto bp = static_cast<int8_t>(pencil.boardPos - 3);
	if(bp < 0)
		bp = 8 + bp%3;
	pencil.boardPos = bp;
}

void movePencilDown()
{
	auto bp = static_cast<int8_t>(pencil.boardPos + 3);
	if(bp > 8)
		bp = (3 + (bp - 11)%3) %3;
	pencil.boardPos = bp;
}

void markCurrentBoardCase()
{
	auto& bc = currentBoardCase();
	if(isEmpty(bc) != true)
		return;
	if(pencil.playerMark == eGamePlayerMark::Circle)
		markCircle(bc);
	else
		markCross(bc);
	pencil.isMarking = true;
	pencil.needSwitchPlayer = true;
}

void updatePencil()
{
	auto& curBoardCase = boardCases[pencil.boardPos];
	pencil.isMarking = isAnimated(curBoardCase);
	if(pencil.isMarking == true)
		return;
	else if(pencil.needSwitchPlayer == true)
		switchPlayer();
}

void drawPencil()
{
	if((pencil.isMarking == true) || (gameState != eGameState::Playing))
		return;
	auto& drawPos = boardCases[pencil.boardPos].pos;
	Z.drawBitmapOver(drawPos.x - 1, drawPos.y - 8, pencil_bmp, pencil_mask, pencil_width, pencil_height);
}

void initBoard()
{
	gameAnimFrames = 4 * 15;
	auto maxShift = int32_t{4};
	board.lines[0].p0.x = static_cast<int16_t>(0  + random(0, maxShift));
	board.lines[0].p0.y = static_cast<int16_t>(20 + random(0, maxShift));
	board.lines[0].p1.x = static_cast<int16_t>(61 + random(0, maxShift));
	board.lines[0].p1.y = static_cast<int16_t>(20 + random(0, maxShift));
	board.lines[1].p0.x = static_cast<int16_t>(0  + random(0, maxShift));
	board.lines[1].p0.y = static_cast<int16_t>(41 + random(0, maxShift));
	board.lines[1].p1.x = static_cast<int16_t>(61 + random(0, maxShift));
	board.lines[1].p1.y = static_cast<int16_t>(41 + random(0, maxShift));
	board.lines[2].p0.x = static_cast<int16_t>(20 + random(0, maxShift));
	board.lines[2].p0.y = static_cast<int16_t>(0  + random(0, maxShift));
	board.lines[2].p1.x = static_cast<int16_t>(20 + random(0, maxShift));
	board.lines[2].p1.y = static_cast<int16_t>(61 + random(0, maxShift));
	board.lines[3].p0.x = static_cast<int16_t>(41 + random(0, maxShift));
	board.lines[3].p0.y = static_cast<int16_t>(0  + random(0, maxShift));
	board.lines[3].p1.x = static_cast<int16_t>(41 + random(0, maxShift));
	board.lines[3].p1.y = static_cast<int16_t>(61 + random(0, maxShift));

	board.winLine.p0 = Point{1, 1};
	board.winLine.p1 = Point{1, 100};
	for(auto& boardCase : boardCases)
		initBoardCase(boardCase);
	initPencil();
	board.needCheckMatch3 = false;
	board.emptyCount = 9;
}

void checkMatch3()
{
	auto hasEmpty = false;
	for(auto n = 0; n < 24; n += 3)
	{
		auto n0 = pgm_read_byte(boardCheck + n);
		auto n1 = pgm_read_byte(boardCheck + n + 1);
		auto n2 = pgm_read_byte(boardCheck + n + 2);
		auto bc0state = boardCases[n0].state;
		auto bc1state = boardCases[n1].state;
		auto bc2state = boardCases[n2].state;
		if(bc0state == eGamePlayerMark::None)
		{
			hasEmpty = true;
			continue;
		}
		if(bc1state == eGamePlayerMark::None)
		{
			hasEmpty = true;
			continue;
		}
		if((bc0state == boardCases[n1].state) && (bc0state == boardCases[n2].state))
		{
			gameState = eGameState::Finished;
			board.winLine.p0 = boardCases[n0].pos;
			board.winLine.p1 = boardCases[n2].pos;
			gameWinner = pencil.playerMark;
			return;
		}
		if(bc2state == eGamePlayerMark::None)
			hasEmpty = true;
	}
	if(hasEmpty == false)
		gameState = eGameState::Finished;
}

void updateBoard()
{
	if(gameAnimFrames > 0)
		--gameAnimFrames;
	else
	{
		if(gameState == eGameState::InitBoard)
			gameState = eGameState::Playing;
	}
	auto bcAnimated = false;
	for(auto& boardCase : boardCases)
	{
		updateBoardCase(boardCase);
		if(isAnimated(boardCase) == true)
			bcAnimated = true;
	}
	if((bcAnimated == false) && (board.needCheckMatch3 == true))
	{
		checkMatch3();
		board.needCheckMatch3 = false;
		--board.emptyCount;
	}
}

void drawBoard()
{
	auto nline = 4;
	if(gameState == eGameState::InitBoard)
		nline = 4 - gameAnimFrames / 15;
	for(auto n = 0; n < nline; ++n)
	{
		auto& line = board.lines[n];
		Z.drawLine(line.p0.x, line.p0.y, line.p1.x, line.p1.y, eColor::White);
	}

	for(auto& boardCase : boardCases)
		drawBoardCase(boardCase);

	if((gameState == eGameState::Finished) && (gameWinner != eGamePlayerMark::None))
	{
		Z.drawLine(board.winLine.p0.x, board.winLine.p0.y, board.winLine.p1.x, board.winLine.p1.y, eColor::White);
		Z.drawLine(board.winLine.p0.x - 1, board.winLine.p0.y, board.winLine.p1.x - 1, board.winLine.p1.y, eColor::White);
		Z.drawLine(board.winLine.p0.x + 1, board.winLine.p0.y, board.winLine.p1.x + 1, board.winLine.p1.y, eColor::White);
		Z.drawLine(board.winLine.p0.x, board.winLine.p0.y - 1, board.winLine.p1.x, board.winLine.p1.y - 1, eColor::White);
		Z.drawLine(board.winLine.p0.x, board.winLine.p0.y + 1, board.winLine.p1.x, board.winLine.p1.y + 1, eColor::White);
	}
}

struct IA
{
	eGamePlayerMark mark;

	void init()
	{
		auto nm = random(4);
		if(nm < 2)
			mark = pencil.playerMark;
		else
			mark = (pencil.playerMark == eGamePlayerMark::Circle)? eGamePlayerMark::Cross : eGamePlayerMark::Circle;
	}

	bool isPlaying() const
	{
		if(gameMode == eGameMode::Vs)
			return false;
		return mark == pencil.playerMark;
	}

	eGamePlayerMark opponentMark() const
	{
		return (mark == eGamePlayerMark::Circle)? eGamePlayerMark::Cross : eGamePlayerMark::Circle;
	}

	void play()
	{
		if(gameMode == eGameMode::VsCPUEasy)
		{
			auto nm = random(4);
			if(nm < 2)
				playForTheBest();
			else
				pencil.boardPos = randomPlay();
		}
		else
		{
			auto rn = random(10);
			if(rn == 0)
			{
				playForTheBest();
			}
			else
			{
				switch(board.emptyCount)
				{
				case 9:
					playInAnyCorner();
					break;

				case 8:
					{
						auto omark = opponentMark();
						if(boardCases[4].state == omark)
							playInAnyCorner();
						else
							pencil.boardPos = 4;
					}
					break;

				case 7:
					{
						auto omark = opponentMark();
						// opponent is in center
						if(boardCases[4].state == omark)
						{
							if(boardCases[0].state == mark)
								pencil.boardPos = 8;
							else if(boardCases[2].state == mark)
								pencil.boardPos = 6;
							else if(boardCases[6].state == mark)
								pencil.boardPos = 2;
							else // should be in 8
								pencil.boardPos = 0;
						}
						else if((boardCases[1].state == omark)
							 || (boardCases[7].state == omark))
						{
							if(boardCases[0].state == mark)
								pencil.boardPos = 6;
							else if(boardCases[2].state == mark)
								pencil.boardPos = 8;
							else if(boardCases[6].state == mark)
								pencil.boardPos = 0;
							else // should be in 8
								pencil.boardPos = 2;
						}
						else if((boardCases[3].state == omark)
							 || (boardCases[5].state == omark))
						{
							if(boardCases[0].state == mark)
								pencil.boardPos = 2;
							else if(boardCases[2].state == mark)
								pencil.boardPos = 0;
							else if(boardCases[6].state == mark)
								pencil.boardPos = 8;
							else // should be in 8
								pencil.boardPos = 6;
						}
						else
							playForTheBest();
					}
					break;

				case 6:
					if(boardCases[4].state == mark)
					{
						auto omark = opponentMark();
						// specific O-X-O config, rarely happen but will win
						if(((boardCases[1].state == omark) && (boardCases[7].state == omark))
						|| ((boardCases[3].state == omark) && (boardCases[5].state == omark)))
							pencil.boardPos = 8;
						// diag O-X-O, trying not to lose!
						else if(((boardCases[0].state == omark) && (boardCases[8].state == omark))
							 || ((boardCases[2].state == omark) && (boardCases[6].state == omark)))
							playAnySide();
						else
							playForTheBest();
					}
					else
						playForTheBest();
					break;

				case 5:
					// check for possible win configuration
					if(isEmpty(boardCases[4]) == true)
						pencil.boardPos = 4;
					else
						playForTheBest();
					break;

				default:
					playForTheBest();
					break;
				}
			}
		}
		markCurrentBoardCase();
	}

	void playForTheBest()
	{
		auto pwin = findPossibleWin();
		if(pwin != -1)
			pencil.boardPos = pwin;
		else
		{
			pwin = findPlayerPossibleWin();
			if(pwin != -1)
				pencil.boardPos = pwin;
			else
				pencil.boardPos = randomPlay();
		}
	}

	finline void playInAnyCorner()
	{
		static uint8_t const pn[] PROGMEM = {0, 2, 6, 8};
		playFromAny4(pn);
	}

	finline void playAnySide()
	{
		static uint8_t const pn[] PROGMEM = {1, 3, 5, 7};
		playFromAny4(pn);
	}

	void playFromAny4(uint8_t const* pn)
	{
		auto nm = random(4);
		while(isEmpty(boardCases[pgm_read_byte(pn + nm)]) == false)
			nm = random(4);
		pencil.boardPos = pgm_read_byte(pn + nm);
	}

	int8_t findPossibleWin() const
	{
		for(auto n = 0; n < 24; n += 3)
		{
			auto n0 = pgm_read_byte(boardCheck + n);
			auto n1 = pgm_read_byte(boardCheck + n + 1);
			auto n2 = pgm_read_byte(boardCheck + n + 2);
			auto bc0state = boardCases[n0].state;
			auto bc1state = boardCases[n1].state;
			auto bc2state = boardCases[n2].state;
			auto omark = opponentMark();
			if((bc0state == omark) || (bc1state == omark) || (bc2state == omark))
				continue;
			if((bc0state == mark) && (bc1state == mark))
				return n2;
			else if((bc0state == mark) && (bc2state == mark))
				return n1;
			else if((bc1state == mark) && (bc2state == mark))
				return n0;
		}
		return -1;
	}

	int8_t findPlayerPossibleWin() const
	{
		for(auto n = 0; n < 24; n += 3)
		{
			auto n0 = pgm_read_byte(boardCheck + n);
			auto n1 = pgm_read_byte(boardCheck + n + 1);
			auto n2 = pgm_read_byte(boardCheck + n + 2);
			auto bc0state = boardCases[n0].state;
			auto bc1state = boardCases[n1].state;
			auto bc2state = boardCases[n2].state;
			if((bc0state == mark) || (bc1state == mark) || (bc2state == mark))
				continue;
			auto omark = opponentMark();
			if((bc0state == omark) && (bc1state == omark))
				return n2;
			else if((bc0state == omark) && (bc2state == omark))
				return n1;
			else if((bc1state == omark) && (bc2state == omark))
				return n0;
		}
		return -1;
	}

	int8_t randomPlay()
	{
		auto rn = random(9);
		auto nn = int8_t{-1};
		while(rn >= 0)
		{
			++nn;
			if(nn >= 9)
				nn = 0;
			if(isEmpty(boardCases[nn]) == true)
				--rn;
		}
		return nn;
	}
};
IA ia;

finline bool buttonPressed(uint8_t button, uint8_t buttonState)
{
	return (!(button & buttonState)) && (button & previousButtonState);
}

finline bool buttonFirstPress(uint8_t button, uint8_t buttonState)
{
	return (button & buttonState) && (!(button & previousButtonState));
}

finline bool handleButtonAnyPress()
{
	auto buttonState = Z.buttonState();
	auto r = (buttonState != 0) && (previousButtonState == 0);
	previousButtonState = buttonState;
	return r;
}

////////////////////////////////////////////////////////////////////////////////
// game logic

void gfStart();
void gfBeginPlay();
void gfPlay();
void gfAbout();

void toStart()
{
	gameAnimFrames = 0;
	gameFunc = gfStart;
}

void toBeginPlay()
{
	auto nm = random(4);
	if(nm < 2)
		pencil.playerMark = eGamePlayerMark::Circle;
	gameWinner = eGamePlayerMark::None;
	gameAnimFrames = 90;
	gameFunc = gfBeginPlay;
}

void toPlay()
{
	initBoard();
	if(gameMode != eGameMode::Vs)
		ia.init();
	gameState = eGameState::InitBoard;
	gameFunc = gfPlay;
}

void toAbout()
{
	gameFunc = gfAbout;
}

void gfStart()
{
	// input
	auto buttonState = Z.buttonState();
	if(gameAnimFrames <= 0)
	{
		if(buttonFirstPress(Z.InputUp, buttonState) == true)
			--menuCursorIndex;
		else if(buttonFirstPress(Z.InputDown, buttonState) == true)
			++menuCursorIndex;
		else if((buttonFirstPress(Z.InputA, buttonState) == true)
			 || (buttonFirstPress(Z.InputB, buttonState) == true))
		{
			gameAnimFrames = 30;
		}
	}
	else
	{
		--gameAnimFrames;
		if(gameAnimFrames <= 0)
		{
			if(menuCursorIndex == 3)
			{
				toAbout();
			}
			else
			{
				gameMode = static_cast<eGameMode>(menuCursorIndex);
				toBeginPlay();
			}
		}
	}
	previousButtonState = buttonState;

	if(menuCursorIndex < 0)
		menuCursorIndex = 3;
	else if(menuCursorIndex > 3)
		menuCursorIndex = 0;

	Z.drawBitmap(1, 0, title_bmpc, title_width, title_height);
	auto x = int16_t{40};
	auto y = int16_t{41};
	// draw cursor
	Z.drawBitmap(x - 6, y + menuCursorIndex * 6, cursor_bmp, cursor_width, cursor_height);

	// draw menu
	// 1P - EASY
	auto hasAnim = (gameAnimFrames <= 0) || ((gameAnimFrames%10) < 5);
	if((menuCursorIndex != 0) || (hasAnim == true))
	{
		drawNumbers(x, y, 1);
		drawTextP(x + 8, y, str_PLAYER);
		Z.drawLine(x + 33, y + 2, x + 34, y + 2, eColor::White);
		drawTextP(x + 37, y, str_EASY);
	}
	// 1P - NORMAL
	if((menuCursorIndex != 1) || (hasAnim == true))
	{
		drawNumbers(x, y + 6, 1);
		drawTextP(x + 8, y + 6, str_PLAYER);
		Z.drawLine(x + 33, y + 8, x + 34, y + 8, eColor::White);
		drawTextP(x + 37, y + 6, str_NORMAL);
	}
	// 2P
	if((menuCursorIndex != 2) || (hasAnim == true))
	{
		drawNumbers(x, y + 12, 2);
		drawTextP(x + 8, y + 12, str_PLAYER);
	}
	// ABOUT
	if((menuCursorIndex != 3) || (hasAnim == true))
		drawTextP(x, y + 18, str_ABOUT);
	// VERSION
	drawNumbers(119, 59, 1);
	Z.drawPixel(123, 63, eColor::White);
	drawNumbers(125, 59, 0);
}

void gfBeginPlay()
{
	--gameAnimFrames;
	if(gameAnimFrames <= 0)
		toPlay();
	if(gameAnimFrames%40 < 20)
	{
		drawPlayerName(36, 29, pencil.playerMark);
		drawTextP(72, 29, str_BEGIN);
	}
}

void gfPlay()
{
	// update input
	if(gameState == eGameState::Finished)
	{
		if(handleButtonAnyPress() == true)
			toStart();
	}
	else
	{
		auto buttonState = Z.buttonState();
		// handle pencil move
		if((gameState == eGameState::Playing) && (pencil.isMarking == false))
		{
			if(ia.isPlaying() == true)
			{
				ia.play();
				board.needCheckMatch3 = true;
			}
			else
			{
				if(buttonFirstPress(Z.InputLeft, buttonState) == true)
					movePencilLeft();
				else if(buttonFirstPress(Z.InputRight, buttonState) == true)
					movePencilRight();
				else if(buttonFirstPress(Z.InputUp, buttonState) == true)
					movePencilUp();
				else if(buttonFirstPress(Z.InputDown, buttonState) == true)
					movePencilDown();
				else if((buttonFirstPress(Z.InputA, buttonState) == true)
					 || (buttonFirstPress(Z.InputB, buttonState) == true))
				{
					markCurrentBoardCase();
					board.needCheckMatch3 = true;
				}
			}
		}
		previousButtonState = buttonState;
	}

	// update
	updateBoard();
	updatePencil();

	// draw
	drawBoard();
	if(gameState == eGameState::Finished)
	{
		if(gameWinner == eGamePlayerMark::None)
			drawTextP(76, 29, str_NOBODY_WINS);
		else
		{
			if(gameMode == eGameMode::Vs)
			{
				drawPlayerName(72, 29, gameWinner);
				drawTextP(108, 29, str_WINS);
			}
			else
			{
				if(gameWinner == ia.mark)
					drawTextP(80, 29, str_YOU_LOSE);
				else
					drawText(80, 29, str_YOU_WIN);
			}
		}
	}
	else
	{
		if(ia.isPlaying() == false)
			drawPencil();
		drawPlayerName(84, 29, pencil.playerMark);
	}
}

void gfAbout()
{
	if(handleButtonAnyPress() == true)
		toStart();
	drawTextP(30, 15, str_MADE_BY);
	drawTextP(64, 23, str_ZEDUCKMASTER);
	drawTextP(30, 35, str_LICENSE);
	drawTextP(64, 43, str_GNU_GPL_V);
	drawNumbers(100, 43, 3);
}

////////////////////////////////////////////////////////////////////////////////
// main program

void zsetup()
{
	// boot
	Z.begin();
	Z.initRandomSeed();
	// start
	toStart();
}

void zloop()
{
	// control frame time
	{
		auto curtime = millis();
		auto elapsedTime = curtime - lasttime;
		if(elapsedTime < FrameTime)
		{
			if((FrameTime - elapsedTime) > 1)
				Z.idle();
			return;
		}
		lasttime = curtime;
		++frame;
	}
	// game func
	gameFunc();
	Z.display();
}


