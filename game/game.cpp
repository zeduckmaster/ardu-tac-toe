#if !defined(ARDUINO_ARCH_AVR)
#include "main.h"

#if defined(__cplusplus)
extern "C"
#endif
int main(int argc, char* argv[])
{
	unused(argc);
	unused(argv);
	if(SDL_Init(SDL_INIT_EVERYTHING) < 0)
	{
		SDL_Log("Failed to initialize SDL (\"%s\").\n", SDL_GetError());
		return -1;
	}
	SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, 0);

	zsetup();
	auto sdlQuit = false;
	while(sdlQuit == false)
	{
		auto sdlEvent = SDL_Event{0};
		while(SDL_PollEvent(&sdlEvent))
			sdlQuit = (sdlEvent.type == SDL_QUIT) || ((sdlEvent.type == SDL_KEYDOWN) && (sdlEvent.key.keysym.scancode == SDL_SCANCODE_ESCAPE));
		zloop();
	}
	return 0;
}
#endif
