#pragma once

#include <stdint.h>

#if defined(__GNUC__)
#define finline __attribute__((always_inline)) inline
#elif defined(_MSC_VER)
#define finline inline
#endif

template<class... T>
finline void unused(T&&...) {}

#if defined(ARDUINO_ARCH_AVR)

#include <Arduino.h>
#include <SPI.h>
#include <avr/sleep.h>
#include <avr/eeprom.h>

#if !defined(cbi)
	#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#if !defined(sbi)
	#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

extern int __heap_start;
extern int* __brkval;

#if defined(ARDUINO_AVR_ARDUBOY)
#if !defined(ARDUBOY_10)
#define ARDUBOY_10
#endif
#endif

#else

#if defined(_WIN32)
#define SDL_MAIN_HANDLED
#include <SDL.h>
#include <SDL_audio.h>
#elif defined(__linux__)
#include <SDL2/SDL.h>
#elif defined(__APPLE__)
#include <SDL.h>
#endif
#include <string>
#include <iostream>
#include <chrono>
#include <queue>
#include <fstream>
#include <cmath>
#include <cstring>
#include <array>

#define PROGMEM
#define _BV(bit) (1<<(bit))

template<typename T>
inline uint8_t pgm_read_byte(T const* p) { return *reinterpret_cast<uint8_t const*>(p); }
template<typename T>
inline uint16_t pgm_read_word(T const* p) { return *reinterpret_cast<uint16_t const*>(p); }

inline void delay(uint32_t ms) { SDL_Delay(ms); }

auto const StartTime = std::chrono::high_resolution_clock::now();
inline uint32_t millis()
{
	auto curTime = std::chrono::high_resolution_clock::now();
	return static_cast<uint32_t>(std::chrono::duration_cast<std::chrono::milliseconds>(curTime - StartTime).count());
}
#if defined(_WIN32)
inline int32_t random() { return rand(); }
#endif

struct _Serial
{
	void begin(uint32_t baud) { unused(baud); }
	template<typename T>
	inline void print(T const& v) { std::cout << v; }
	template<typename T>
	inline void println(T const& v) { std::cout << v << std::endl; }
	inline void flush() {}
};
_Serial Serial;

void sdlAudioCallback(void* userData, Uint8* stream, int len);

class AudioTone
{
public:
	static auto constexpr Amplitude = 28000;
	static auto constexpr Frequency = 44100;

	void init()
	{
		auto desiredSpec = SDL_AudioSpec{};
		desiredSpec.freq = Frequency;
		desiredSpec.format = AUDIO_S16SYS;
		desiredSpec.channels = 1;
		desiredSpec.samples = 2048;
		desiredSpec.callback = sdlAudioCallback;
		desiredSpec.userdata = this;
		auto obtainedSpec = SDL_AudioSpec{};
		_devId = SDL_OpenAudioDevice(nullptr, 0, &desiredSpec, &obtainedSpec, 0);
	}

	void shutdown()
	{
		SDL_CloseAudioDevice(_devId);
	}

	void enable(bool value)
	{
		SDL_LockAudioDevice(_devId);
		SDL_PauseAudioDevice(_devId, (value == true)? 0 : 1);
		if(value == false)
			_tones.clear();
		SDL_UnlockAudioDevice(_devId);
	}

	void tone(double freq, int duration)
	{
		SDL_LockAudioDevice(_devId);
		if(SDL_GetAudioDeviceStatus(_devId) == SDL_AUDIO_PLAYING)
			_tones.push_back(_Tone{freq, duration * Frequency / 1000});
		SDL_UnlockAudioDevice(_devId);
	}

	void tones(uint16_t const* data)
	{
		auto curData = data;
		SDL_LockAudioDevice(_devId);
		if(SDL_GetAudioDeviceStatus(_devId) == SDL_AUDIO_PLAYING)
		{
			while(*curData != 0x8000)
			{
				auto freq = static_cast<double>(*curData);
				++curData;
				auto duration = static_cast<int>(*curData);
				++curData;
				_tones.push_back(_Tone{freq, duration * Frequency / 1000});
			}
		}
		SDL_UnlockAudioDevice(_devId);
	}

	void genSamples(Sint16* stream, int length)
	{
		auto i = 0;
		while(i < length)
		{
			if(_tones.empty())
			{
				while(i < length)
				{
					stream[i] = 0;
					++i;
				}
				return;
			}
			auto& tone = _tones.front();
			auto samplesToDo = std::min(i + tone.samplesLeft, length);
			tone.samplesLeft -= samplesToDo - i;
			while(i < samplesToDo)
			{
				stream[i] = static_cast<Sint16>(static_cast<double>(Amplitude) * std::sin(_v * 2 * M_PI / Frequency));
				++i;
				_v += tone.freq;
			}
			if(tone.samplesLeft == 0)
				_tones.pop_front();
		}
	}

private:
	struct _Tone
	{
		double freq;
		int samplesLeft;

		_Tone(double freq, int samplesLeft)
			: freq{freq}
			, samplesLeft{samplesLeft}
		{}
	};
	SDL_AudioDeviceID _devId = 0;
	double _v = 0;
	std::deque<_Tone> _tones;
};

void sdlAudioCallback(void* userData, Uint8* stream, int len)
{
	auto a = reinterpret_cast<AudioTone*>(userData);
	a->genSamples(reinterpret_cast<Sint16*>(stream), len / 2);
}

#endif

#undef max
#undef min

//template <typename T>
//void PROGMEM_readAny(T const* src, T& dest)
//{
//	memcpy_P(&dest, src, sizeof(T));
//}
//
//template <typename T> T PROGMEM_getAnything (const T * sce)
//  {
//  static T temp;
//  memcpy_P (&temp, sce, sizeof (T));
//  return temp;
//  }

template<typename T>
finline T const& max(T const& a, T const& b) { return (a < b)? b : a; }

template<typename T>
finline T const& min(T const& a, T const& b) { return (a < b)? a : b; }

template<typename T>
finline void swap(T& a, T& b)
{
	auto t = a;
	a = b;
	b = t;
}

template<typename T>
finline T clamp(T a, T amin, T amax)
{
	return min<T>(max<T>(a, amin), amax);
}

finline int32_t random(int32_t max) { return random() % max; }

finline int32_t random(int32_t min, int32_t max) { return random() % (max - min) + min; }

enum eColor : uint8_t
{
	Black = 0x00,
	White = 0xff
};

class ZArduboy
{
public:
	static constexpr auto const InputUp    = uint8_t{_BV(7)};
	static constexpr auto const InputRight = uint8_t{_BV(6)};
	static constexpr auto const InputDown  = uint8_t{_BV(4)};
	static constexpr auto const InputLeft  = uint8_t{_BV(5)};
	static constexpr auto const InputA     = uint8_t{_BV(3)};
	static constexpr auto const InputB     = uint8_t{_BV(2)};
	static constexpr auto const LCDWidth   = int16_t{128};
	static constexpr auto const LCDHeight  = int16_t{64};
	static constexpr auto const LCDRAMSize = int16_t{1024}; // 128*64/8
	static constexpr auto const EEPROMSize = uint16_t{1024};

public:
	finline void boot()
	{
#if defined(ARDUBOY_10)
		_spiBegin();

		// pins boot - optimized
		{
			// up, right, left, down
			auto mode = reinterpret_cast<uint8_t*>(0x30); // DDRF
			auto out = reinterpret_cast<uint8_t*>(0x31); // PORTF
			auto bit = uint8_t{_BV(7)|_BV(6)|_BV(5)|_BV(4)}; // A0,A1,A2,A3
			*mode &= ~bit;
			*out |= bit;
			// A
			mode = reinterpret_cast<uint8_t*>(0x2D); // DDRE
			out = reinterpret_cast<uint8_t*>(0x2E); // PORTE
			bit = _BV(6); // D7
			*mode &= ~bit;
			*out |= bit;
			// B
			mode = reinterpret_cast<uint8_t*>(0x24); // DDRB
			out = reinterpret_cast<uint8_t*>(0x25); // PORTB
			bit = _BV(4); // D8
			*mode &= ~bit;
			*out |= bit;
			// DC, CS, RST
			mode = reinterpret_cast<uint8_t*>(0x2A); // DDRD
			out = reinterpret_cast<uint8_t*>(0x2B); // PORTD
			bit = _BV(4)|_BV(6)|_BV(7); // D4, D12, D6
			*mode |= bit;
			// LED_RED, LED_GREEN, LED_BLUE
			mode = reinterpret_cast<uint8_t*>(0x24); // DDRB
			out = reinterpret_cast<uint8_t*>(0x25); // PORTB
			bit = _BV(6)|_BV(7)|_BV(5); // D10, D11, D9
			*mode |= bit;
		}

		// reset
		{
			auto pin = _kRSTPin;
			auto bit = _kPinToBit[pin];
			auto port = _kPinToPort[pin];
			auto out = reinterpret_cast<volatile uint8_t*>(_kPortToOutput[port]);
			auto timer = _kPinToTimer[pin];
			if(timer != NOT_ON_TIMER)
				_turnOffPWM(timer);
			*out |= bit; // write HIGH to RST
			delay(1);
			*out &= ~bit; // write LOW to RST
			delay(10);
			*out |= bit; // write HIGH to RST
		}

		// boot lcd
		{
			uint8_t const lcdBoot[] =
			{
				// boot defaults are commented out but left here incase they
				// might prove useful for reference
				// further reading: https://www.adafruit.com/datasheets/SSD1306.pdf
				// display Off
				// 0xAE,
				// set Display Clock Divisor v = 0xF0
				// default is 0x80
				0xD5, 0xF0,
				// set Multiplex Ratio v = 0x3F
				// 0xA8, 0x3F,
				// set Display Offset v = 0
				// 0xD3, 0x00,
				// set Start Line (0)
				// 0x40,
				// charge Pump Setting v = enable (0x14)
				// default is disabled
				0x8D, 0x14,
				// set Segment Re-map (A0) | (b0001)
				// default is (b0000)
				0xA1,
				// set COM Output Scan Direction
				0xC8,
				// set COM Pins v
				// 0xDA, 0x12,
				// set Contrast v = 0xCF
				0x81, 0xCF,
				// set Precharge = 0xF1
				0xD9, 0xF1,
				// set VCom Detect
				// 0xDB, 0x40,
				// entire Display ON
				// 0xA4,
				// set normal/inverse display
				// 0xA6,
				// display On
				0xAF,
				// set display mode = horizontal addressing mode (0x00)
				0x20, 0x00,
				// set col address range
				// 0x21, 0x00, COLUMN_ADDRESS_END,
				// set page address range
				// 0x22, 0x00, PAGE_ADDRESS_END
			};
			_spiSetClockDivider(SPI_CLOCK_DIV2);
			_lcdCommandMode();
			for(auto n=uint8_t{0}; n < sizeof(lcdBoot); ++n)
				_spiTransfer(lcdBoot[n]);
			_lcdDataMode();
		}

		// save much power
		{
			_power_adc_disable();
			_power_usart0_disable();
			_power_twi_disable();
			// timer 0 is for millis()
			// timers 1 and 3 are for music and sounds
			_power_timer2_disable();
			_power_usart1_disable();
			// we need USB, for now (to allow triggered reboots to reprogram)
			// power_usb_disable()
		}
		// sound
		auto toneOut = reinterpret_cast<volatile uint8_t*>(_kPortToOutput[_kPinToPort[_kTonePin]]);
		auto toneMode = reinterpret_cast<volatile uint8_t*>(_kPortToMode[_kPinToPort[_kTonePin]]);
		auto tonePinMask = _kPinToBit[_kTonePin];
		*toneOut &= ~tonePinMask;
		*toneMode |= tonePinMask;
		auto toneOut2 = reinterpret_cast<volatile uint8_t*>(_kPortToOutput[_kPinToPort[_kTonePin2]]);
		auto toneMode2 =reinterpret_cast<volatile uint8_t*>(_kPortToMode[_kPinToPort[_kTonePin2]]);
		auto tonePinMask2 = _kPinToBit[_kTonePin2];
		*toneOut2 &= ~tonePinMask2;
		*toneMode2 |= tonePinMask2;
#else
		// eeprom
		_eeprom.resize(EEPROMSize);
		std::fstream in{"eeprom.bin", std::ios_base::in | std::ios_base::binary};
		if(in.fail() == false)
			in.read(reinterpret_cast<char*>(_eeprom.data()), EEPROMSize);
		// graphic
		SDL_CreateWindowAndRenderer(LCDWidth * 4, LCDHeight * 4, 0, &_window, &_renderer);
		if((_window == nullptr) || (_renderer == nullptr))
		{
			SDL_Log("failed to init display (\"%s\").\n", SDL_GetError());
			return;
		}
		SDL_RenderSetLogicalSize(_renderer, LCDWidth, LCDHeight);
		SDL_SetRenderDrawColor(_renderer, 255, 255, 255, 255);
		SDL_RenderClear(_renderer);
		// sound
		_audio.init();
		_audio.enable(_isSoundEnabled);
#endif
	}

	finline void begin()
	{
		boot();
		setRGBled(0, 0, 0);
	}

	finline uint8_t buttonState() const
	{
#if defined(ARDUBOY_10)
		uint8_t state = ((~PINF) & B11110000); // up, right, left, down
		state |= (((~PINE) & B01000000) >> 3); // A
		state |= (((~PINB) & B00010000) >> 2); // B
		return state;
#else
		auto ks = SDL_GetKeyboardState(nullptr);
		auto state = uint8_t{0};
		state = (ks[SDL_SCANCODE_UP])? InputUp : 0;
		state |= (ks[SDL_SCANCODE_RIGHT])? InputRight : 0;
		state |= (ks[SDL_SCANCODE_LEFT])? InputLeft : 0;
		state |= (ks[SDL_SCANCODE_DOWN])? InputDown : 0;
		state |= (ks[SDL_SCANCODE_A])? InputA : 0;
		state |= (ks[SDL_SCANCODE_B])? InputB : 0;
		return state;
#endif
	}

	void idle()
	{
#if defined(ARDUBOY_10)
		set_sleep_mode(SLEEP_MODE_IDLE);
		sleep_mode();
#else
		SDL_Delay(1);
#endif
	}

	void display()
	{
#if defined(ARDUBOY_10)
		for(auto n = int16_t{0}; n < LCDRAMSize; ++n)
		{
			SPDR = _frameBuffer[n];
			_frameBuffer[n] = 0;
			while(!(SPSR & _BV(SPIF))); // wait
		}
#else
		auto n = size_t{0};
		for(auto y = int16_t{0}; y < LCDHeight; y += 8)
		{
			for(auto x = int16_t{0}; x < LCDWidth; ++x)
			{
				auto col = _frameBuffer[n];
				_frameBuffer[n++] = 0;
				for(auto i = 0; i < 8; ++i)
				{
					auto pixel = col & (1 << i);
					if(pixel == 0)
						SDL_SetRenderDrawColor(_renderer, 0, 0, 0, 255);
					else
						SDL_SetRenderDrawColor(_renderer, 255, 255, 255, 255);
					SDL_RenderDrawPoint(_renderer, x, y+i);
				}
			}
		}
		SDL_RenderPresent(_renderer);
#endif
	}

	void setRGBled(uint8_t red, uint8_t green, uint8_t blue)
	{
#if defined(ARDUBOY_10)
		// inversion is necessary because these are common annode LEDs
		sbi(TCCR1A, COM1B1);
		OCR1B = 255 - red;
		sbi(TCCR0A, COM0A1);
		OCR0A = 255 - green;
		sbi(TCCR1A, COM1A1);
		OCR1A = 255 - blue;
#else
		_ledRed = red;
		_ledGreen = green;
		_ledBlue = blue;
#endif
	}

	void initRandomSeed()
	{
#if defined(ARDUBOY_10)
		_power_adc_enable(); // ADC on
		randomSeed(~_rawADC(_kADC_Temp) * ~_rawADC(_kADC_Voltage) * ~micros() + micros());
		_power_adc_disable(); // ADC off
#else
		srand(static_cast<uint32_t>(time(nullptr)));
#endif
	}

	void drawBitmap(int16_t x, int16_t y, uint8_t const* bitmap, uint8_t w, uint8_t h)
	{
		if(((x+w) <= 0) || (x > (LCDWidth-1)) || ((y+h) <= 0) || (y > (LCDHeight-1)))
			return;

		auto yOffset = int16_t(abs(y) % 8);
		auto sRow = y / 8;
		if(y < 0)
		{
			--sRow;
			yOffset = 8 - yOffset;
		}
		auto rows = int8_t(h/8);
		if((h%8) != 0)
			++rows;
		for(auto a = int8_t{0}; a < rows; ++a)
		{
			auto bRow = sRow + a;
			if(bRow > ((LCDHeight/8)-1))
				break;
			if(bRow <= -2)
				continue;
			for(auto iCol = int8_t{0}; iCol < w; ++iCol)
			{
				if((iCol + x) > (LCDWidth-1))
					break;
				if((iCol + x) < 0)
					continue;
				auto color = pgm_read_byte(bitmap + (a*w) + iCol);
				if(bRow >= 0)
					_frameBuffer[ (bRow*LCDWidth) + x + iCol ] |= (color << yOffset);
				if((yOffset != 0) && (bRow < ((LCDHeight/8)-1)))
					_frameBuffer[ ((bRow+1)*LCDWidth) + x + iCol ] |= (color >> (8-yOffset));
			}
		}
	}

	void drawBitmapEx(int16_t x, int16_t y, uint8_t const* bitmap, uint8_t w, uint8_t h, bool flip)
	{
		if(((x+w) <= 0) || (x > (LCDWidth-1)) || ((y+h) <= 0) || (y > (LCDHeight-1)))
			return;

		auto yOffset = int16_t(abs(y) % 8);
		auto sRow = y / 8;
		if(y < 0)
		{
			--sRow;
			yOffset = 8 - yOffset;
		}
		auto rows = int8_t(h/8);
		if((h%8) != 0)
			++rows;
		for(auto a = int8_t{0}; a < rows; ++a)
		{
			auto bRow = sRow + a;
			if(bRow > ((LCDHeight/8)-1))
				break;
			if(bRow <= -2)
				continue;

			for(auto col = int8_t{0}; col < w; ++col)
			{
				if((col + x) > (LCDWidth-1))
					break;
				if((col + x) < 0)
					continue;
				auto color = pgm_read_byte(bitmap + (a*w) + ((flip==true)? w-col-1 : col));
				if(bRow >= 0)
					_frameBuffer[ (bRow*LCDWidth) + x + col ] |= (color << yOffset);
				if((yOffset != 0) && (bRow < ((LCDHeight/8)-1)))
					_frameBuffer[ ((bRow+1)*LCDWidth) + x + col ] |= (color >> (8-yOffset));
			}
		}
	}

	void drawBitmapOver(int16_t x, int16_t y, uint8_t const* bitmap, uint8_t const* mask, uint8_t w, uint8_t h)
	{
		if(((x + w) <= 0) || (x > (LCDWidth - 1)) || ((y + h) <= 0) || (y > (LCDHeight - 1)))
			return;
		// xOffset technically doesn't need to be 16 bit but the math operations
		// are measurably faster if it is
		auto yOffset = static_cast<int8_t>(abs(y) % 8);
		auto sRow = static_cast<int8_t>(y / 8);
		if((y < 0) && (yOffset > 0))
		{
			--sRow;
			yOffset = 8 - yOffset;
		}
		// if the left side of the render is offscreen skip those loops
		auto xOffset = static_cast<uint16_t>((x<0)? abs(x) : 0);
		// if the right side of the render is offscreen skip those loops
		auto rendered_width = static_cast<uint8_t>(((x + w) > (LCDWidth - 1))? ((LCDWidth - x) - xOffset) : (w - xOffset));
		// if the top side of the render is offscreen skip those loops
		auto start_h = static_cast<uint8_t>((sRow < -1)? abs(sRow) - 1 : 0);
		auto loop_h = static_cast<uint8_t>(h / 8 + (h % 8 > 0 ? 1 : 0)); // divide, then round up
		// if (sRow + loop_h - 1 > (HEIGHT/8)-1)
		if((sRow + loop_h) > (LCDHeight / 8))
			loop_h = static_cast<uint8_t>((LCDHeight / 8) - sRow);
		// prepare variables for loops later so we can compare with 0
		// instead of comparing two variables
		loop_h -= start_h;
		sRow += start_h;
		auto ofs = static_cast<uint16_t>((sRow * LCDWidth) + x + xOffset);
		auto bofs = bitmap + (start_h * w) + xOffset;
		auto mul_amt = static_cast<uint8_t>(1 << yOffset);

		if(mask == nullptr)
		{
			// we only want to mask the 8 bits of our own sprite, so we can
			// calculate the mask before the start of the loop
			auto mask_data = static_cast<uint16_t>(~(0xFF * mul_amt));
			// really if yOffset = 0 you have a faster case here that could be
			// optimized
			for(auto a = uint8_t{0}; a < loop_h; ++a)
			{
				for(auto iCol = uint8_t{0}; iCol < rendered_width; ++iCol)
				{
					auto bitmap_data = uint16_t(pgm_read_byte(bofs) * mul_amt);
					if(sRow >= 0)
					{
						auto data = _frameBuffer[ofs];
						data &= static_cast<uint8_t>(mask_data);
						data |= static_cast<uint8_t>(bitmap_data);
						_frameBuffer[ofs] = data;
					}
					if((yOffset != 0) && (sRow < 7))
					{
						auto tmp = static_cast<uint16_t>(ofs + LCDWidth);
						auto data = _frameBuffer[tmp];
						data &= *( reinterpret_cast<uint8_t*>(&mask_data) + 1);
						data |= *( reinterpret_cast<uint8_t*>(&bitmap_data) + 1);
						_frameBuffer[tmp] = data;
					}
					++ofs;
					++bofs;
				}
				++sRow;
				bofs += w - rendered_width;
				ofs += LCDWidth - rendered_width;
			}
		}
		else
		{
			auto mask_ofs = const_cast<uint8_t*>(mask) + (start_h * w) + xOffset;
			for(auto a = uint8_t{0}; a < loop_h; ++a)
			{
				for(auto iCol = uint8_t{0}; iCol < rendered_width; ++iCol)
				{
					// NOTE: you might think in the yOffset==0 case that this results
					// in more effort, but in all my testing the compiler was forcing
					// 16-bit math to happen here anyways, so this isn't actually
					// compiling to more code than it otherwise would. If the offset
					// is 0 the high part of the word will just never be used.

					// load data and bit shift
					// mask needs to be bit flipped
					uint16_t mask_data = ~(pgm_read_byte(mask_ofs) * mul_amt);
					uint16_t bitmap_data = pgm_read_byte(bofs) * mul_amt;
					if(sRow >= 0)
					{
						uint8_t data = _frameBuffer[ofs];
						data &= static_cast<uint8_t>(mask_data);
						data |= static_cast<uint8_t>(bitmap_data);
						_frameBuffer[ofs] = data;
					}
					if((yOffset != 0) && (sRow < 7))
					{
						auto tmp = uint16_t(ofs + LCDWidth);
						uint8_t data = _frameBuffer[tmp];
						data &= (*(reinterpret_cast<unsigned char *>(&mask_data) + 1));
						data |= (*(reinterpret_cast<unsigned char *>(&bitmap_data) + 1));
						_frameBuffer[tmp] = data;
					}
					ofs++;
					mask_ofs++;
					bofs++;
				}
				sRow++;
				bofs += w - rendered_width;
				mask_ofs += w - rendered_width;
				ofs += LCDWidth - rendered_width;
			}
		}
	}

	void drawCompressed(int16_t sx, int16_t sy, uint8_t const* bitmap, uint8_t w, uint8_t h)
	{
		// no need to draw at all if we're offscreen
		if((sx + w < 0) || (sx >= LCDWidth) || (sy + h < 0) || (sy >= LCDHeight))
			return;
		// set up decompress state
		_cs.src = bitmap;
		_cs.bit = 0x100;
		_cs.byte = 0;
		_cs.src_pos = 0;
		// sy = sy - (frame*h);
		auto yOffset = abs(sy) % 8;
		auto sRow = static_cast<int8_t>(sy / 8);
		if(sy < 0)
		{
			--sRow;
			yOffset = 8 - yOffset;
		}
		auto rows = h / 8;
		if((h%8) != 0)
			++rows;
		auto a = int8_t{0}; // +(frame*rows);
		auto iCol = int16_t{0};
		auto byte = uint8_t{0};
		auto bit = uint16_t{1};
		auto col = _getval(1); // starting colour
		while(a < rows) // + (frame*rows))
		{
			auto bl = uint8_t{1};
			while(!_getval(1))
				bl += 2;
			auto len = _getval(bl) + 1; // span length
			// draw the span
			for(auto i = 0; i < len; ++i)
			{
				if(col)
					byte |= bit;
				bit <<= 1;
				if(bit == 0x100) // reached end of byte
				{
					// draw
					auto bRow = static_cast<int8_t>(sRow + a);
					//if (byte) // possible optimisation
					if((bRow <= (LCDHeight / 8) - 1)
					   && (bRow > -2)
					   && (iCol + sx <= (LCDWidth - 1))
					   && (iCol + sx >= 0))
					{
						if(bRow >= 0)
							_frameBuffer[(bRow * LCDWidth) + sx + iCol] |= byte << yOffset;
						//if((yOffset) && (bRow < (LCDHeight / 8) - 1) && (bRow > -2))
						if(yOffset)
							_frameBuffer[((bRow + 1)*LCDWidth) + sx + iCol] |= byte >> (8 - yOffset);
					}
					// iterate
					++iCol;
					if(iCol >= w)
					{
						iCol = 0;
						++a;
					}
					// reset byte
					byte = 0;
					bit = 1;
				}
			}
			col = 1 - col; // toggle colour for next span
		}
	}

	void drawPixel(int16_t x, int16_t y, uint8_t color)
	{
		if((x < 0) || (y < 0) || (x >= LCDWidth) || (y >= LCDHeight))
			return;
		auto pos = (y / 8) * LCDWidth + x;
		if(color == eColor::White)
			_frameBuffer[pos] |= _BV(y % 8);
		else
			_frameBuffer[pos] &= ~_BV(y % 8);
	}

	void drawLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint8_t color)
	{
		auto steep = abs(y1 - y0) > abs(x1 - x0);
		if(steep)
		{
			swap(x0, y0);
			swap(x1, y1);
		}
		if(x0 > x1)
		{
			swap(x0, x1);
			swap(y0, y1);
		}

		auto dx = x1 - x0;
		auto dy = abs(y1 - y0);
		auto err = dx >> 1;
		auto ystep = (y0 < y1) ? 1 : -1;
		for(; x0 <= x1; ++x0)
		{
			auto x = (steep) ? y0 : x0;
			auto y = (steep) ? x0 : y0;
			drawPixel(x, y, color);
			err -= dy;
			if(err < 0)
			{
				y0 += ystep;
				err += dx;
			}
		}
	}

	void drawHLine(int16_t x0, int16_t y, int16_t x1, uint8_t color)
	{
		auto start = max(x0, int16_t{0});
		auto end = min(x1, int16_t{LCDWidth});
		for(auto xpos = start; xpos <= end; ++xpos)
			drawPixel(xpos, y, color);
	}

	void drawVLine(int16_t x, int16_t y0, int16_t y1, uint8_t color)
	{
		auto start = max(y0, int16_t{0});
		auto end = min(y1, int16_t{LCDHeight});
		for(auto ypos = start; ypos <= end; ++ypos)
			drawPixel(x, ypos, color);
	}

	void drawRect(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint8_t color)
	{
		drawHLine(x0, y0, x1, color);
		drawHLine(x0, y1, x1, color);
		drawVLine(x0, y0+1, y1-1, color);
		drawVLine(x1, y0+1, y1-1, color);
	}

	void drawCircle(int16_t centerX, int16_t centerY, uint8_t radius, eColor color)
	{
		drawPixel(centerX, centerY + radius, color);
		drawPixel(centerX, centerY - radius, color);
		drawPixel(centerX + radius, centerY, color);
		drawPixel(centerX - radius, centerY, color);

		auto f = static_cast<int16_t>(1 - radius);
		auto x = int16_t{0};
		auto y = int16_t{radius};
		auto ddF_x = int16_t{1};
		auto ddF_y = static_cast<int16_t>(-2 * radius);
		while(x < y)
		{
			if(f >= 0)
			{
				--y;
				ddF_y += 2;
				f += ddF_y;
			}
			++x;
			ddF_x += 2;
			f += ddF_x;

			drawPixel(centerX + x, centerY + y, color);
			drawPixel(centerX - x, centerY + y, color);
			drawPixel(centerX + x, centerY - y, color);
			drawPixel(centerX - x, centerY - y, color);
			drawPixel(centerX + y, centerY + x, color);
			drawPixel(centerX - y, centerY + x, color);
			drawPixel(centerX + y, centerY - x, color);
			drawPixel(centerX - y, centerY - x, color);
		}
	}

	void enableSound(bool value)
	{
		if(_isSoundEnabled == value)
			return;
		_isSoundEnabled = value;
#if !defined(ARDUBOY_10)
		_audio.enable(value);
#endif
	}

	finline bool isSoundEnabled()
	{
		return _isSoundEnabled;
	}

	void tones(uint16_t const* data)
	{
#if defined(ARDUBOY_10)
		bitWrite(TIMSK3, OCIE3A, 0); // disable the output compare match interrupt
		_tonesIndex = const_cast<uint16_t*>(data); // set to start of sequence array
		nextTone();
#else
		_audio.tones(data);
#endif
	}

	void writeToEEPROM(uint16_t pos, uint8_t value)
	{
#if defined(ARDUBOY_10)
		eeprom_write_byte(reinterpret_cast<uint8_t*>(pos), value);
#else
		_eeprom[pos] = value;
		std::fstream out{"eeprom.bin", std::ios_base::out | std::ios_base::binary};
		out.write(reinterpret_cast<char const*>(_eeprom.data()), EEPROMSize);
#endif
	}

	uint8_t readFromEEPROM(uint16_t pos)
	{
#if defined(ARDUBOY_10)
		return eeprom_read_byte(reinterpret_cast<uint8_t*>(pos));
#else
		return _eeprom[pos];
#endif
	}

	void updateToEEPROM(uint16_t pos, uint8_t value)
	{
		if(readFromEEPROM(pos) != value)
			writeToEEPROM(pos, value);
	}

private:
	uint8_t _frameBuffer[LCDRAMSize];
	static volatile bool _isSoundEnabled;

	// for compressed bitmaps
	struct _CSESSION
	{
		int byte;
		int bit;
		uint8_t const* src;
		int src_pos;
	};
	_CSESSION _cs;

	int16_t _getval(uint8_t bits)
	{
		auto val = int16_t{0};
		for(auto i = uint8_t{0}; i < bits; ++i)
		{
			if(_cs.bit == 0x100)
			{
				_cs.bit = 0x1;
				_cs.byte = pgm_read_byte(&_cs.src[_cs.src_pos]);
				++_cs.src_pos;
			}
			if(_cs.byte & _cs.bit)
				val += (1 << i);
			_cs.bit <<= 1;
		}
		return val;
	}

#if defined(ARDUBOY_10)

public:
	static void nextTone()
	{
		auto toneOut = reinterpret_cast<volatile uint8_t*>(_kPortToOutput[_kPinToPort[_kTonePin]]);
		auto tonePinMask = _kPinToBit[_kTonePin];
		uint16_t freq = pgm_read_word(_tonesIndex++);
		if(freq == 0x8000)
		{
			bitWrite(TIMSK3, OCIE3A, 0);
			TCCR3B = 0; // stop the counter
			*toneOut &= ~tonePinMask; // set the pin low
			return;
		}
		uint32_t ocrValue;
		if(freq == 0)
		{
			// if tone is silent
			ocrValue = F_CPU / 8 / 250 / 2 - 1; // dummy tone for silence
			freq = 250;
			_toneIsSilent = true;
			*toneOut &= ~tonePinMask; // set the pin low
		}
		else
		{
			ocrValue = F_CPU / 8 / freq / 2 - 1;
			_toneIsSilent = false;
		}
		if(_isSoundEnabled == false)
			_toneIsSilent = true;

		uint16_t dur = pgm_read_word(_tonesIndex++);
		long toggleCount;
		if(dur != 0)
		{
			// A right shift is used to divide by 512 for efficency.
			// For durations in milliseconds it should actually be a divide by 500,
			// so durations will by shorter by 2.34% of what is specified.
			toggleCount = ((long)dur * freq) >> 9;
		}
		else
			toggleCount = -1; // indicate infinite duration

		TCCR3A = 0;
		TCCR3B = _BV(WGM32) | _BV(CS31); // CTC mode, prescaler /8
		OCR3A = ocrValue;
		_toneSampleCount = toggleCount;
		bitWrite(TIMSK3, OCIE3A, 1); // enable the output compare match interrupt
	}

	static void updateSound()
	{
		auto toneOut = reinterpret_cast<volatile uint8_t*>(_kPortToOutput[_kPinToPort[_kTonePin]]);
		auto tonePinMask = _kPinToBit[_kTonePin];

		if(_toneSampleCount != 0)
		{
			if(_toneIsSilent == false)
				*toneOut ^= tonePinMask; // toggle the pin
			if(_toneSampleCount > 0)
				--_toneSampleCount;
		}
		else
		{
			nextTone();
		}
	}
private:
	static volatile int32_t _toneSampleCount;
	static volatile bool _toneIsSilent;
	static volatile uint16_t* _tonesIndex;
private:
	static constexpr auto const _kDCPin = uint8_t{4};
	static constexpr auto const _kCSPin = uint8_t{12};
	static constexpr auto const _kRSTPin = uint8_t{6};
	static constexpr auto const _kTonePin = uint8_t{5};
	static constexpr auto const _kTonePin2 = uint8_t{13};
	// compare Vcc to 1.1 bandgap
	static constexpr auto const _kADC_Voltage = (_BV(REFS0) | _BV(MUX4) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1));
	// compare temperature to 2.5 internal reference and _BV(MUX5)
	static constexpr auto const _kADC_Temp = (_BV(REFS0) | _BV(REFS1) | _BV(MUX2) | _BV(MUX1) | _BV(MUX0));

	static constexpr uint8_t const _kPinToBit[] =
	{
		_BV(2), // D0 - PD2
		_BV(3),	// D1 - PD3
		_BV(1), // D2 - PD1
		_BV(0),	// D3 - PD0
		_BV(4),	// D4 - PD4
		_BV(6), // D5 - PC6
		_BV(7), // D6 - PD7
		_BV(6), // D7 - PE6

		_BV(4), // D8 - PB4
		_BV(5),	// D9 - PB5
		_BV(6), // D10 - PB6
		_BV(7),	// D11 - PB7
		_BV(6), // D12 - PD6
		_BV(7), // D13 - PC7

		_BV(3),	// D14 - MISO - PB3
		_BV(1),	// D15 - SCK - PB1
		_BV(2),	// D16 - MOSI - PB2
		_BV(0),	// D17 - SS - PB0

		_BV(7),	// D18 - A0 - PF7
		_BV(6), // D19 - A1 - PF6
		_BV(5), // D20 - A2 - PF5
		_BV(4), // D21 - A3 - PF4
		_BV(1), // D22 - A4 - PF1
		_BV(0), // D23 - A5 - PF0

		_BV(4), // D24 / D4 - A6 - PD4
		_BV(7), // D25 / D6 - A7 - PD7
		_BV(4), // D26 / D8 - A8 - PB4
		_BV(5), // D27 / D9 - A9 - PB5
		_BV(6), // D28 / D10 - A10 - PB6
		_BV(6), // D29 / D12 - A11 - PD6
		_BV(5)  // D30 / TX Led - PD5
	};

	static constexpr uint8_t const _kPinToPort[] =
	{
		4, // D0 - PD2
		4, // D1 - PD3
		4, // D2 - PD1
		4, // D3 - PD0
		4, // D4 - PD4
		3, // D5 - PC6
		4, // D6 - PD7
		5, // D7 - PE6

		2, // D8 - PB4
		2, // D9 - PB5
		2, // D10 - PB6
		2, // D11 - PB7
		4, // D12 - PD6
		3, // D13 - PC7

		2, // D14 - MISO - PB3
		2, // D15 - SCK - PB1
		2, // D16 - MOSI - PB2
		2, // D17 - SS - PB0

		6, // D18 - A0 - PF7
		6, // D19 - A1 - PF6
		6, // D20 - A2 - PF5
		6, // D21 - A3 - PF4
		6, // D22 - A4 - PF1
		6, // D23 - A5 - PF0

		4, // D24 / D4 - A6 - PD4
		4, // D25 / D6 - A7 - PD7
		2, // D26 / D8 - A8 - PB4
		2, // D27 / D9 - A9 - PB5
		2, // D28 / D10 - A10 - PB6
		4, // D29 / D12 - A11 - PD6
		4  // D30 / TX Led - PD5
	};

	static constexpr uint8_t const _kPortToMode[] =
	{
		0,
		0,
		0x24, // DDRB
		0x27, // DDRC
		0x2A, // DDRD
		0x2D, // DDRE
		0x30  // DDRF
	};

	static constexpr uint8_t const _kPortToOutput[] =
	{
		0,
		0,
		0x25, // PORTB
		0x28, // PORTC
		0x2B, // PORTD
		0x2E, // PORTE
		0x31  // PORTF
	};

	static constexpr uint8_t const _kPortToInput[] =
	{
		0,
		0,
		0x23, // PINB
		0x26, // PINC
		0x29, // PIND
		0x2C, // PINE
		0x2F  // PINF
	};

	static constexpr uint8_t const _kPinToTimer[] =
	{
		NOT_ON_TIMER,
		NOT_ON_TIMER,
		NOT_ON_TIMER,
		TIMER0B,		// 3
		NOT_ON_TIMER,
		TIMER3A,		// 5
		TIMER4D,		// 6
		NOT_ON_TIMER,

		NOT_ON_TIMER,
		TIMER1A,		// 9
		TIMER1B,		// 10
		TIMER0A,		// 11

		NOT_ON_TIMER,
		TIMER4A,		// 13

		NOT_ON_TIMER,
		NOT_ON_TIMER,
		NOT_ON_TIMER,
		NOT_ON_TIMER,
		NOT_ON_TIMER,
		NOT_ON_TIMER,

		NOT_ON_TIMER,
		NOT_ON_TIMER,
		NOT_ON_TIMER,
		NOT_ON_TIMER,
		NOT_ON_TIMER,
		NOT_ON_TIMER,
		NOT_ON_TIMER,
		NOT_ON_TIMER,
		NOT_ON_TIMER,
		NOT_ON_TIMER,
		NOT_ON_TIMER,
	};

	finline void _turnOffPWM(uint8_t timer)
	{
		switch(timer)
		{
#if defined(TCCR1A) && defined(COM1A1)
		case TIMER1A:   cbi(TCCR1A, COM1A1);    break;
#endif
#if defined(TCCR1A) && defined(COM1B1)
		case TIMER1B:   cbi(TCCR1A, COM1B1);    break;
#endif
#if defined(TCCR1A) && defined(COM1C1)
		case TIMER1C:   cbi(TCCR1A, COM1C1);    break;
#endif

#if defined(TCCR2) && defined(COM21)
		case  TIMER2:   cbi(TCCR2, COM21);      break;
#endif

#if defined(TCCR0A) && defined(COM0A1)
		case  TIMER0A:  cbi(TCCR0A, COM0A1);    break;
#endif

#if defined(TCCR0A) && defined(COM0B1)
		case  TIMER0B:  cbi(TCCR0A, COM0B1);    break;
#endif
#if defined(TCCR2A) && defined(COM2A1)
		case  TIMER2A:  cbi(TCCR2A, COM2A1);    break;
#endif
#if defined(TCCR2A) && defined(COM2B1)
		case  TIMER2B:  cbi(TCCR2A, COM2B1);    break;
#endif

#if defined(TCCR3A) && defined(COM3A1)
		case  TIMER3A:  cbi(TCCR3A, COM3A1);    break;
#endif
#if defined(TCCR3A) && defined(COM3B1)
		case  TIMER3B:  cbi(TCCR3A, COM3B1);    break;
#endif
#if defined(TCCR3A) && defined(COM3C1)
		case  TIMER3C:  cbi(TCCR3A, COM3C1);    break;
#endif

#if defined(TCCR4A) && defined(COM4A1)
		case  TIMER4A:  cbi(TCCR4A, COM4A1);    break;
#endif
#if defined(TCCR4A) && defined(COM4B1)
		case  TIMER4B:  cbi(TCCR4A, COM4B1);    break;
#endif
#if defined(TCCR4A) && defined(COM4C1)
		case  TIMER4C:  cbi(TCCR4A, COM4C1);    break;
#endif
#if defined(TCCR4C) && defined(COM4D1)
		case TIMER4D:	cbi(TCCR4C, COM4D1);	break;
#endif

#if defined(TCCR5A)
		case  TIMER5A:  cbi(TCCR5A, COM5A1);    break;
		case  TIMER5B:  cbi(TCCR5A, COM5B1);    break;
		case  TIMER5C:  cbi(TCCR5A, COM5C1);    break;
#endif
		}
	}

	finline void _pinModeOutput(uint8_t pin)
	{
		auto port = _kPinToPort[pin];
		auto bit = _kPinToBit[pin];
		auto reg = reinterpret_cast<volatile uint8_t*>(_kPortToMode[port]);
		*reg |= bit;
	}

	void _spiBegin()
	{
		auto sreg = SREG;
		noInterrupts(); // protect from a scheduler and prevent transactionBegin
		// set SS to high so a connected chip will be "deselected" by default
		// when the SS pin is set as OUTPUT, it can be used as
		// a general purpose output port (it doesn't influence
		// SPI operations).
		_pinModeOutput(SS);

		// warning: if the SS pin ever becomes a LOW INPUT then SPI
		// automatically switches to Slave, so the data direction of
		// the SS pin MUST be kept as OUTPUT.
		SPCR |= _BV(MSTR);
		SPCR |= _BV(SPE);

		// set direction register for SCK and MOSI pin.
		// MISO pin automatically overrides to INPUT.
		// by doing this AFTER enabling SPI, we avoid accidentally
		// clocking in a single bit since the lines go directly
		// from "input" to SPI control.
		// http://code.google.com/p/arduino/issues/detail?id=888
		_pinModeOutput(SCK);
		_pinModeOutput(MOSI);

		SREG = sreg;
	}

	void _spiTransfer(uint8_t data)
	{
		SPDR = data;
		asm volatile("nop");
		while(!(SPSR & _BV(SPIF))); // wait
	}

	finline void _spiSetClockDivider(uint8_t clockDiv)
	{
		auto const kSPIClockMask = uint8_t{0x03}; // SPR1 = bit 1, SPR0 = bit 0 on SPCR
		auto const kSPI2XClockMask = uint8_t{0x01}; // SPI2X = bit 0 on SPSR
		SPCR = (SPCR & ~kSPIClockMask) | (clockDiv & kSPIClockMask);
		SPSR = (SPSR & ~kSPI2XClockMask) | ((clockDiv >> 2) & kSPI2XClockMask);
	}

	void _spiSetDataMode(uint8_t dataMode)
	{
		auto kSPIModeMask = uint8_t{0x0C}; // CPOL = bit 3, CPHA = bit 2 on SPCR
		SPCR = (SPCR & ~kSPIModeMask) | dataMode;
	}

	finline void _power_adc_disable() { (PRR0 |= (uint8_t)(1 << PRADC)); }
	finline void _power_adc_enable() { (PRR0 &= (uint8_t)~(1 << PRADC)); }
	finline void _power_usart0_disable() { (PRR0 |= (uint8_t)(1 << PRUSART0)); }
	finline void _power_twi_disable() { (PRR0 |= (uint8_t)(1 << PRTWI)); }
	finline void _power_timer2_disable() { (PRR0 |= (uint8_t)(1 << PRTIM2)); }
	finline void _power_usart1_disable() { (PRR1 |= (uint8_t)(1 << PRUSART1)); }

	void _lcdCommandMode()
	{
		auto csOut = reinterpret_cast<volatile uint8_t*>(_kPortToOutput[_kPinToPort[_kCSPin]]);
		auto csBit = _kPinToBit[_kCSPin];
		auto dcOut = reinterpret_cast<volatile uint8_t*>(_kPortToOutput[_kPinToPort[_kDCPin]]);
		auto dcBit = _kPinToBit[_kDCPin];
		*csOut |= csBit; // CS HIGH
		*dcOut &= ~dcBit; // DC LOW
		*csOut &= ~csBit; // CS LOW
	}

	void _lcdDataMode()
	{
		auto csOut = reinterpret_cast<volatile uint8_t*>(_kPortToOutput[_kPinToPort[_kCSPin]]);
		auto csBit = _kPinToBit[_kCSPin];
		auto dcOut = reinterpret_cast<volatile uint8_t*>(_kPortToOutput[_kPinToPort[_kDCPin]]);
		auto dcBit = _kPinToBit[_kDCPin];
		//*csOut |= csBit; // CS HIGH needed?
		*dcOut |= dcBit; // DC HIGH
		*csOut &= ~csBit; // CS LOW
	}

	uint16_t _rawADC(uint8_t adc_bits)
	{
		ADMUX = adc_bits;
		// we also need MUX5 for temperature check
		if(adc_bits == _kADC_Temp)
			ADCSRB = _BV(MUX5);
		delay(2); // Wait for ADMUX setting to settle
		ADCSRA |= _BV(ADSC); // Start conversion
		while(bit_is_set(ADCSRA, ADSC)); // measuring
		return ADC;
	}

#else

private:
	SDL_Window* _window = nullptr;
	SDL_Renderer* _renderer = nullptr;
	AudioTone _audio;
	std::vector<uint8_t> _eeprom;
	uint8_t _ledRed = 0;
	uint8_t _ledGreen = 0;
	uint8_t _ledBlue = 0;
#endif
};

enum eNote : uint16_t
{
	Note_Rest = 0,
	Note_C0   = 16,
	Note_CS0  = 17,
	Note_D0   = 18,
	Note_DS0  = 19,
	Note_E0   = 21,
	Note_F0   = 22,
	Note_FS0  = 23,
	Note_G0   = 25,
	Note_GS0  = 26,
	Note_A0   = 28,
	Note_AS0  = 29,
	Note_B0   = 31,
	Note_C1   = 33,
	Note_CS1  = 35,
	Note_D1   = 37,
	Note_DS1  = 39,
	Note_E1   = 41,
	Note_F1   = 44,
	Note_FS1  = 46,
	Note_G1   = 49,
	Note_GS1  = 52,
	Note_A1   = 55,
	Note_AS1  = 58,
	Note_B1   = 62,
	Note_C2   = 65,
	Note_CS2  = 69,
	Note_D2   = 73,
	Note_DS2  = 78,
	Note_E2   = 82,
	Note_F2   = 87,
	Note_FS2  = 93,
	Note_G2   = 98,
	Note_GS2  = 104,
	Note_A2   = 110,
	Note_AS2  = 117,
	Note_B2   = 123,
	Note_C3   = 131,
	Note_CS3  = 139,
	Note_D3   = 147,
	Note_DS3  = 156,
	Note_E3   = 165,
	Note_F3   = 175,
	Note_FS3  = 185,
	Note_G3   = 196,
	Note_GS3  = 208,
	Note_A3   = 220,
	Note_AS3  = 233,
	Note_B3   = 247,
	Note_C4   = 262,
	Note_CS4  = 277,
	Note_D4   = 294,
	Note_DS4  = 311,
	Note_E4   = 330,
	Note_F4   = 349,
	Note_FS4  = 370,
	Note_G4   = 392,
	Note_GS4  = 415,
	Note_A4   = 440,
	Note_AS4  = 466,
	Note_B4   = 494,
	Note_C5   = 523,
	Note_CS5  = 554,
	Note_D5   = 587,
	Note_DS5  = 622,
	Note_E5   = 659,
	Note_F5   = 698,
	Note_FS5  = 740,
	Note_G5   = 784,
	Note_GS5  = 831,
	Note_A5   = 880,
	Note_AS5  = 932,
	Note_B5   = 988,
	Note_C6   = 1047,
	Note_CS6  = 1109,
	Note_D6   = 1175,
	Note_DS6  = 1245,
	Note_E6   = 1319,
	Note_F6   = 1397,
	Note_FS6  = 1480,
	Note_G6   = 1568,
	Note_GS6  = 1661,
	Note_A6   = 1760,
	Note_AS6  = 1865,
	Note_B6   = 1976,
	Note_C7   = 2093,
	Note_CS7  = 2218,
	Note_D7   = 2349,
	Note_DS7  = 2489,
	Note_E7   = 2637,
	Note_F7   = 2794,
	Note_FS7  = 2960,
	Note_G7   = 3136,
	Note_GS7  = 3322,
	Note_A7   = 3520,
	Note_AS7  = 3729,
	Note_B7   = 3951,
	Note_C8   = 4186,
	Note_CS8  = 4435,
	Note_D8   = 4699,
	Note_DS8  = 4978,
	Note_E8   = 5274,
	Note_F8   = 5588,
	Note_FS8  = 5920,
	Note_G8   = 6272,
	Note_GS8  = 6645,
	Note_A8   = 7040,
	Note_AS8  = 7459,
	Note_B8   = 7902,
	Note_C9   = 8372,
	Note_CS9  = 8870,
	Note_D9   = 9397,
	Note_DS9  = 9956,
	Note_E9   = 10548,
	Note_F9   = 11175,
	Note_FS9  = 11840,
	Note_G9   = 12544,
	Note_GS9  = 13290,
	Note_A9   = 14080,
	Note_AS9  = 14917,
	Note_B9   = 15804,
	Note_End  = 0x8000
};
volatile bool ZArduboy::_isSoundEnabled = false;

#if defined(ARDUBOY_10)

volatile int32_t ZArduboy::_toneSampleCount = 0;
volatile bool ZArduboy::_toneIsSilent = true;
volatile uint16_t* ZArduboy::_tonesIndex = nullptr;

extern "C" void TIMER3_COMPA_vect(void) __attribute__ ((signal,__INTR_ATTRS));
void TIMER3_COMPA_vect(void) { ZArduboy::updateSound(); }

#endif
