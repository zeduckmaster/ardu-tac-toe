#pragma once

#include "zarduboy.h"

char const str_PLAYER[] PROGMEM = "PLAYER";
char const str_NOBODY_WINS[] PROGMEM = "NOBODY WINS";
char const str_WINS[] PROGMEM = "WINS";
char const str_YOU_LOSE[] PROGMEM = "YOU LOSE";
char const str_YOU_WIN[] PROGMEM = "YOU WIN";
char const str_BEGIN[] PROGMEM = "BEGIN";
char const str_EASY[] PROGMEM = "EASY";
char const str_NORMAL[] PROGMEM = "NORMAL";
char const str_ABOUT[] PROGMEM = "ABOUT";
char const str_ARDU_TAC_TOE[] PROGMEM = "ARDU TAC TOE";
char const str_MADE_BY[] PROGMEM = "MADE BY";
char const str_ZEDUCKMASTER[] PROGMEM = "ZEDUCKMASTER";
char const str_LICENSE[] PROGMEM = "LICENSE";
char const str_GNU_GPL_V[] PROGMEM = "GNU GPL V";
char const str_PRESS_ANY_KEY[] PROGMEM = "PRESS ANY BUTTON TO CONTINUE";

