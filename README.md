![ardu-tac-toe](https://framagit.org/zeduckmaster/ardu-tac-toe/raw/master/screen0.png "ardu-tac-toe screenshot")

# How to play

The game follows the rules of Tic-Tac-Toe with different play mode: 1 player (easy/normal) or 2 players.

![ardu-tac-toe](https://framagit.org/zeduckmaster/ardu-tac-toe/raw/master/screen1.png "ardu-tac-toe screenshot")

Use D-Pad (arrows on desktop) to move the pencil, and A/B to mark the board case. On desktop, you can quit using *Escape* key.

# Binaries

* arduboy: [ardu-tac-toe-1.0.hex](/uploads/2ab6d1a13a7b2ca6a2b6b1f3bd8a68a6/ardu-tac-toe-1.0.hex) + [ardu-tac-toe-1.0.arduboy](/uploads/da71d8ff646c92c6b81ddffec521e37d/ardu-tac-toe-1.0.arduboy)
* windows: [ardu-tac-toe-1.0.7z](/uploads/e681bd30d7ad67bbd605f702db71cd25/ardu-tac-toe-1.0.7z)
* linux 64bits: [ardu-tac-toe-1.0-x86_64.AppImage](https://acloud.zaclys.com/index.php/s/HHToHd7cKL3bGX3?path=%2Fardu-tac-toe)

# How to build

## Arduboy

The game has been compiled with the official Arduino ide 1.8.7 and tested successfuly on Arduboy v1.

## Desktop

The game can be compiled for windows, linux and macOS with the provided Qt Creator project.

Building for macOS and linux require to install SDL2 framework.

# Information

License: GPLv3: https://www.gnu.org/licenses/gpl-3.0.en.html
